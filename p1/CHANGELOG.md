# Changelog
All notable changes to this project will be documented based on  this file.
Firmware : P1 : v1.0.8 : 2020-07-23

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Error codes to MQTT server - Method stable, Error codes' definition file is under progress. (2020-07-25).

## [1.0.8] - 2020-07-23
### Added
- Time sync from *MQTT-time* topic (the topic gets refreshed each minute) [@bhavesh](https://gitlab.com/0b001000001000).

### Changed
- Verbose logging to filesystem disabled.
- ISR() - interrupt service routine made more stable by using portMUX_INITIALIZER_UNLOCKED method as suggested in IDF forums.

### Removed
- Time sync struct - structure for getting time from HTTPS time api. Current method involves pointers rewrite.

## [1.0.7] - 2020-07-22
### Changed
- Timer Groups for network synced device timer and pulse timer.

### Fixed
- Minor bug fixes and optimisations

## [1.0.6] - 2020-07-20
### Fixed
- Minor bug fixes for MQTT protocol and filesystem after feedback from field tests.

## [1.0.5] - 2020-07-15
### Added
- FOTA update from remote CDN fetch
- server commands remote update device from MQTT topic ******
- Device configuration files for deviceID and Client's wifi credentials

### Changed
- Total device control from server - disabled.

## [1.0.4] - 2020-07-15
### Added
- Certificate verification for secure communications
- Topic based login method to access and send data to MQTT broker
- Server side commands functionality added for testing. Paves way for remote FOTA update.

### Changed
- MQTT configured for secure trasaction
- Message max payload limit from 128 bytes to 256 bytes

## [1.0.3] - 2020-07-12
### Fixed
- Minor bug fixes for MQTT protocol and filesystem after feedback from field tests.

## [1.0.2] - 2020-07-11
### Fixed
- Minor bug fixes for MQTT protocol and filesystem after feedback from field tests.

## [1.0.1] - 2020-07-10
### Added
- MQTT protocol for communication with server and data transactions (unsecure)

### Changed
- Pulse logging method: previously a datapacket of 30 pulse was uploaded, new method sends transmission every pulse.
- Filesytem method for logging device / system events.

### Removed
- HTTPS method for communication with server and data transactions

## [1.0.0] - 2020-07-07
### INIT
- HTTPS method for communication with server and data transactions